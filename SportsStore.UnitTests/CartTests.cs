﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportsStore.Domain.Entities;
using System.Linq;
using Moq;
using SportsStore.Domain.Abstract;
using SportsStore.WebUI.Controllers;
using System.Web.Mvc;
using SportsStore.WebUI.Models;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class CartTests
    {

        [TestMethod]
        public void Can_Delete_Valid_Products()
        {

            // Arrange - create a Product
            Product prod = new Product { ProductID = 2, Name = "Test" };

            // Arrange - create the mock repository
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] { new Product { ProductID = 1, Name = "P1" }, prod, new Product { ProductID = 3, Name = "P3" }, });

            // Arrange - create the controller
            AdminController target = new AdminController(mock.Object);

            // Act - delete the product
            target.Delete(prod.ProductID);
        } 
    
            // Assert - ensure that the repository delete method was    // called with the correct Product    mock.Verify(m => m.DeleteProduct(prod.ProductID)); } 
            [TestMethod]
        public void Cannot_Checkout_Empty_Cart()
        {

            // Arrange - create a mock order processor
            Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
            // Arrange - create an empty cart
            Cart cart = new Cart();
            // Arrange - create shipping details
            ShippingDetails shippingDetails = new ShippingDetails();
            // Arrange - create an instance of the controller
            CartController target = new CartController(null, mock.Object);

            // Act
            ViewResult result = target.Checkout(cart, shippingDetails);

            // Assert - check that the order hasn't been passed on to the processor
            mock.Verify(m => m.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Never());
            // Assert - check that the method is returning the default view
            Assert.AreEqual("", result.ViewName);
            // Assert - check that I am passing an invalid model to the view
            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }
        //[TestMethod]
        //    public void Can_Add_To_Cart()
        //    {
        //        // Arrange - create the mock repository
        //        Mock<IProductRepository> mock = new Mock<IProductRepository>();  
        //        mock.Setup(m => m.Products).Returns(new Product[]
        //        {
        //            new Product {ProductID = 1, Name = "P1", Category = "Apples"},      
        //        }.AsQueryable()
        //        );

        //        // Arrange - create a Cart
        //        Cart cart = new Cart();

        //        // Arrange - create the controller
        //        CartController target = new CartController(mock.Object);

        //        // Act - add a product to the cart
        //        target.AddToCart(cart, 1, null);

        //        // Assert
        //        Assert.AreEqual(cart.Lines.Count(), 1);    
        //        Assert.AreEqual(cart.Lines.ToArray()[0].Product.ProductID, 1);  
        //    }

        //        [TestMethod] 
        //    public void Adding_Product_To_Cart_Goes_To_Cart_Screen()
        //        {            
        //        // Arrange - create the mock repository
        //        Mock<IProductRepository> mock = new Mock<IProductRepository>();   
        //        mock.Setup(m => m.Products).Returns(new Product[]
        //        {
        //            new Product {ProductID = 1, Name = "P1", Category = "Apples"},    
        //        }.AsQueryable());

        //            // Arrange - create a Cart
        //            Cart cart = new Cart();

        //            // Arrange - create the controller
        //            CartController target = new CartController(mock.Object);

        //            // Act - add a product to the cart
        //            RedirectToRouteResult result = target.AddToCart(cart, 2, "myUrl");

        //            // Assert
        //            Assert.AreEqual(result.RouteValues["action"], "Index");     
        //            Assert.AreEqual(result.RouteValues["returnUrl"], "myUrl");  
        //    }

        //            [TestMethod]      
        //    public void Can_View_Cart_Contents()
        //            {        
        //        // Arrange - create a Cart
        //                   Cart cart = new Cart();

        //                // Arrange - create the controller
        //                CartController target = new CartController(null);
        //                // Act - call the Index action method
        //                CartIndexViewModel result = (CartIndexViewModel)target.Index(cart, "myUrl").ViewData.Model;

        //                // Assert
        //                Assert.AreSame(result.Cart, cart);   
        //                Assert.AreEqual(result.ReturnUrl, "myUrl");      
        //    }
        //}
    } 
}
 