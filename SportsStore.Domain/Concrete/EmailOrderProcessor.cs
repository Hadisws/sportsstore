﻿using System.Net;
using System.Net.Mail;
using System.Text;
using SportsStore.Domain.Abstract;
using SportsStore.Domain.Entities;

namespace SportsStore.Domain.Concrete
{

    public class EmailSettings 
    { 
        public string MailToAddress = "orders@example.com";
        public string MailFromAddress = "sportsstore@example.com";
        public bool UseSsl = true;
        public string Username = "MySmtpUsername";
        public string Password = "MySmtpPassword"; 
        public string ServerName = "smtp.example.com";
        public int ServerPort = 587;
        public bool WriteAsFile = false;
        public string FileLocation = @"C:\Users\FBC\source\repos\SportsStorenew";
    }

    public class EmailOrderProcessor : IOrderProcessor
    {
        private EmailSettings emailSettings;

        public EmailOrderProcessor(EmailSettings settings)
        {
            emailSettings = settings;
        }
        public void ProcessOrder(Cart cart, ShippingDetails shippingInfo)
        {

            using (var smtpClient = new SmtpClient())
            {

                smtpClient.EnableSsl = emailSettings.UseSsl;
                smtpClient.Host = emailSettings.ServerName;
                smtpClient.Port = emailSettings.ServerPort; 
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(emailSettings.Username, emailSettings.Password);

                if (emailSettings.WriteAsFile) 
                { 
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    smtpClient.PickupDirectoryLocation = emailSettings.FileLocation;
                    smtpClient.EnableSsl = false; 
                }

                StringBuilder body = new StringBuilder().AppendLine("A new order has been submitted").AppendLine("---").AppendLine("Items:");

                foreach (var line in cart.Lines) 
                { 
                    var subtotal = line.Product.Price * line.Quantity;
                    body.AppendFormat("{0} x {1} (subtotal: {2:c})\n", 
                        line.Quantity, line.Product.Name,
                        subtotal); 
                }

                body.AppendFormat("\nTotal order value: {0:c}\n", cart.ComputeTotalValue()).AppendLine("---").AppendLine("\nShip to:").AppendLine("Name: "+shippingInfo.Name).AppendLine("Address: "+shippingInfo.Line1+shippingInfo.Line2 +shippingInfo.Line3 ?? "").AppendLine("City: "+shippingInfo.City).AppendLine("State: "+shippingInfo.State ?? "").AppendLine("Country: "+shippingInfo.Country).AppendLine("Zip: "+shippingInfo.Zip).AppendLine("\n---").AppendFormat("Gift wrap: {0}", shippingInfo.GiftWrap ? "Yes" : "No");

                MailMessage mailMessage = new MailMessage(emailSettings.MailFromAddress, // From
                    emailSettings.MailToAddress,// To
                    "New order submitted!",    // Subject
                     body.ToString());        // Body

                if (emailSettings.WriteAsFile)
                    { 
                    mailMessage.BodyEncoding = Encoding.ASCII;
                }

                smtpClient.Send(mailMessage);
            }
        }
    }
}
